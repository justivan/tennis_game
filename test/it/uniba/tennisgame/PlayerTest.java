package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void scoreShouldBeIncreased() {

		//Arrange
		Player p = new Player("Federer", 0);
		
		//Act
		p.incrementScore();
		
		//Assert
		assertEquals(1, p.getScore());
		
	}

	@Test
	public void scoreShouldNotBeIncreased() {

		//Arrange
		Player p = new Player("Federer", 0);
		
		//Act
		
		
		//Assert
		assertEquals(0, p.getScore());
		
	}

	@Test
	public void scoreShouldBeLove() {

		//Arrange
		Player p = new Player("Federer", 0);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertEquals("love", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldBeFifteen() {

		//Arrange
		Player p = new Player("Federer", 1);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertEquals("fifteen", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldBeThirty() {

		//Arrange
		Player p = new Player("Federer", 2);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertEquals("thirty", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldBeForty() {

		//Arrange
		Player p = new Player("Federer", 3);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertEquals("forty", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldBeNullIfNegative() {

		//Arrange
		Player p = new Player("Federer", -1);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertNull(scoreAsString);
		
	}
	
	@Test
	public void scoreShouldBeNullIfBiggerThanThree() {

		//Arrange
		Player p = new Player("Federer", 4);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertNull(scoreAsString);
		
	}
	
	@Test
	public void shouldBeTie() {

		//Arrange
		Player p1 = new Player("Federer", 2);
		Player p2 = new Player("Minguccio", 2);
		
		//Act
		boolean tie = p1.isTieWith(p2);
		
		//Assert
		assertTrue(tie);
		
	}
	
	@Test
	public void shouldNotBeTie() {

		//Arrange
		Player p1 = new Player("Federer", 2);
		Player p2 = new Player("Minguccio", 3);
		
		//Act
		boolean tie = p1.isTieWith(p2);
		
		//Assert
		assertFalse(tie);
		
	}
	
	@Test
	public void shouldHaveAtLeastFortyPoints() {

		//Arrange
		Player p = new Player("Federer", 3);
		
		//Act
		boolean outcome = p.hasAtLeastFortyPoints();
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void shouldNotHaveAtLeastFortyPoints() {

		//Arrange
		Player p = new Player("Federer", 2);
		
		//Act
		boolean outcome = p.hasAtLeastFortyPoints();
		
		//Assert
		assertFalse(outcome);
		
	}
	
	@Test
	public void shouldHaveLessThanFortyPoints() {

		//Arrange
		Player p = new Player("Federer", 2);
		
		//Act
		boolean outcome = p.hasLessThanFortyPoints();
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void shouldNotHaveLessThanFortyPoints() {

		//Arrange
		Player p = new Player("Federer", 3);
		
		//Act
		boolean outcome = p.hasLessThanFortyPoints();
		
		//Assert
		assertFalse(outcome);
		
	}
	
	@Test
	public void shouldHaveMoreThanFortyPoints() {

		//Arrange
		Player p = new Player("Federer", 4);
		
		//Act
		boolean outcome = p.hasMoreThanFourtyPoints();
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void shouldNotHaveMoreThanFortyPoints() {

		//Arrange
		Player p = new Player("Federer", 3);
		
		//Act
		boolean outcome = p.hasMoreThanFourtyPoints();
		
		//Assert
		assertFalse(outcome);
		
	}
	
	@Test
	public void shouldHaveOnePointInAdvantage() {

		//Arrange
		Player p1 = new Player("Federer", 4);
		Player p2 = new Player("Minguccio", 3);
		
		//Act
		boolean outcome = p1.hasOnePointAdvantageOn(p2);
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void shouldNotHaveOnePointInAdvantage() {

		//Arrange
		Player p1 = new Player("Federer", 3);
		Player p2 = new Player("Minguccio", 4);
		
		//Act
		boolean outcome = p1.hasOnePointAdvantageOn(p2);
		
		//Assert
		assertFalse(outcome);
		
	}
	
	@Test
	public void shouldHaveTwoPointInAdvantage() {

		//Arrange
		Player p1 = new Player("Federer", 5);
		Player p2 = new Player("Minguccio", 3);
		
		//Act
		boolean outcome = p1.hasAtLeastTwoPointsAdvantageOn(p2);
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void shouldNotHaveTwoPointInAdvantage() {

		//Arrange
		Player p1 = new Player("Federer", 3);
		Player p2 = new Player("Minguccio", 5);
		
		//Act
		boolean outcome = p1.hasAtLeastTwoPointsAdvantageOn(p2);
		
		//Assert
		assertFalse(outcome);
		
	}
	
}
